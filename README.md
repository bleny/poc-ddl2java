Ce projet est une preuve de concept qui montre qu'on peut générer une interface
de programmation fortement typée pour accéder à la persistence d'une base
simplement à partir de la définition du schéma de la base de données en DDL.

La construction du projet

  - démarre une base postgresql embarquée
  - applique les scripts de migration pour mettre la base dans un état déterminé
  - appelle j00q pour générer l'ensemble du code Java à partir du schéma

Cette approche permet d'éliminer tout risque d'incohérence entre le schéma
et le code Java. La seule source de vérité sont les scripts de migrations.

Cette approche est rapide pour le développeur, il suffit d'écrire la migration :
pas d'entité à écrire, pas de modèle à modifier, pas de fichier .hbm.xml... etc.

Cette approche est sûre puisque qu'à chaque phase de compilation et de test,
les scripts de migrations sont joués : on est donc bien certain qu'en production
ces même scripts aboutiront à un schéma fonctionnel.
