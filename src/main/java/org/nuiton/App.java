package org.nuiton;

import org.jooq.DSLContext;
import org.jooq.Result;
import org.jooq.SQLDialect;
import org.jooq.codegen.maven.example.tables.records.LutinRecord;
import org.jooq.impl.DSL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static org.jooq.codegen.maven.example.Tables.LUTIN;

public class App {

    public static void main(String[] args) throws SQLException {
        String userName = "taiste";
        String password = "taiste";
        String url = "jdbc:postgresql:taiste";

        // Connection is the only JDBC resource that we need
        // PreparedStatement and ResultSet are handled by jOOQ, internally
        try (Connection conn = DriverManager.getConnection(url, userName, password)) {
            DSLContext create = DSL.using(conn, SQLDialect.POSTGRES);
            Result<LutinRecord> fetch = create
                    .selectFrom(LUTIN)
                    .where(LUTIN.FIRSTNAME.contains("Aurore"))
                    .orderBy(LUTIN.LASTNAME)
                    .fetch();

            for (LutinRecord lutinRecord : fetch) {
                System.out.println(lutinRecord.getFirstname() + " " + lutinRecord.getLastname());
            }
        }
    }
}
